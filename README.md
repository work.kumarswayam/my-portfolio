I am a software developer pursuing a Bachelor's in Computer Application at Kalinga University, Raipur. Skilled in Java, Python, C++, and web development frameworks, I am passionate about innovative software solutions and problem-solving. Experienced in full-stack development, algorithms, and database management, I thrive in challenging environments. My goal is to drive innovation in the tech industry, particularly in AI and machine learning.

For Contact 
+91 782-710-2155
work.kumarswayam@gmail.com
LinkedIn: kumarswayam